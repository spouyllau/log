---
date: "2023-06-04"
author1: "Stéphane Pouyllau"
author2: "Claude François"
language: fr
type: billet
abstract: "Ce billet décrit comment équiper un site Web avec une sémantique RDFa."
tags: ["RDFa", "Web sémantique", "Web 3.0", "RDF"]
keywords: ["RDFa", "Web sémantique", "W3C"]
publisher: "CNRS"
title: "RDFa dans une page web"
---

Ce billet décrit comment équiper un site Web avec une structure sémantique dite en RDFa[^RDFa] ; permettant de proposer des informations dans le _Linked Open Data_[^lod] et en particulier pour permettant d'indexer des contenus dans le moteur de recherche [ISIDORE](https://isidore.science).

[^lod]: Le _Linked Open Data_ est …
[^RDFa]: Pour _Resource Description Framework in Attributs_
