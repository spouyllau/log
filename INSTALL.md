# Installation d'un site Web sous Hugo et déployé par GitLab _Pages_

[[_TOC_]]

Cette documentation permet d'utiliser l'instance [GitLab de la TGIR huma-num](https://gitlab.huma-num.fr) pour mettre en œuvre un site Web simple utilisant le moteur de site [Hugo](https://gohugo.io).

## Création du projet/dépôt sous GitLab

Dans un nouveau projet/dépôt, choisir : `New project > Create from template`

![Create from template](img/H001.png)

Choisir : `Pages/Hugo`

![Pages/Hugo](img/H002.png)

Il s'agit de renseigner le :
- Nom du projet,  (_Project name_) ;
- Le marqueur _slug_ (_Project slug_), qui fera partie de l'URL publique du site ;
- La description du projet de site ;
- De positionner le _Visibility Level_ à _Public_ afin que le site soit publié et visible.

![Pages/Hugo](img/H003.png)

GitLab installe Hugo (dont deux thèmes : _Beautifulhugo_ et _Lanyon_) et prépare l'automatisation de l'intégration continue (GitLab CI) pour _Pages_.

### Configuration du site : `config.toml`

Une fois le site Hugo installé. Il faut configurer le fichier `config.toml`.

L'URL du site créé est de la forme : `https://[nom du compte].gitpages.huma-num.fr/[slug]`.

> ATTENTION : dans l'instance d'Huma-Num, il est pas possible d'utiliser un nom de domaine propre.

Ainsi, dans config.toml, `baseurl` doit être :

```toml
baseurl = "https://[nom du compte].gitpages.huma-num.fr/[slug]"
```
Les autres paramètres (label du site, etc.) sont a modifier :

```toml
baseurl = "https://[nom du compte].gitpages.huma-num.fr/[slug]"
contentdir    = "content"
layoutdir     = "layouts"
publishdir    = "public"
title = "Label du site Web"
canonifyurls  = true

DefaultContentLanguage = "fr"
theme = "beautifulhugo"
metaDataFormat = "yaml"
pygmentsUseClasses = true
pygmentCodeFences = true
#disqusShortname = "XXX"
#googleAnalytics = "XXX"

[Params]
  subtitle = "Phrase d'accroche"
  #logo = "img/avatar-icon.png"
  #favicon = "img/favicon.ico"
  dateFormat = "January 2, 2006"
  commit = false
  rss = true
  comments = true
```

### Motorisation avec GitLab CI : `.gitlab-ci.yml`

L'utilisation du _template_ Pages/Hugo installe automatique le script d'intégration continue pour GitLab (disponible en faisant : CI/CD > Editor). Nous ajoutons un processus de test (test:) qui, s'il est positif, autorise le déploiement via _Pages_. Dans le cadre de l'instance d'Huma-Num, le _Runner_ Alpine est disponible, nous l'utilisons pour cet exemple :

```yaml
# All available Hugo versions are listed here: https://gitlab.com/pages/hugo/container_registry
image: registry.gitlab.com/pages/hugo:latest
variables:
  GIT_SUBMODULE_STRATEGY: recursive

test:
  tags:
  - alpine
  script:
  - hugo
  artifacts:
    paths:
      - test
  except:
  - master

pages:
  dependencies:
  - test
  tags:
  - alpine
  script:
  - hugo
  artifacts:
    paths:
    - public
  only:
  - master
```
## Comptabilité des métadonnées du site Web avec un moissonnage par ISIDORE

En suivant [la documentation d'ISIDORE sur le moissonnage via des métadonnées RDFA](https://documentation.huma-num.fr/isidore/#comment-signaler-ses-donnees-dans-isidore-avec-des-metadonnees-rdfa), il est possible d'ajouter dans `head_custom.html`des différents _templates_ d'Hugo, le code `HTML` suivant afin de rendre compatible le site sous Hugo pour un moissonnage et une indexation des contenus par [ISIDORE](https://documentation.huma-num.fr/isidore/#comment-faire-pour-que-des-donnees-soient-referencees-par-isidore) :

```Hugo
<!-- RDFa for ISIDORE -->
<link rel="dcterms:identifier" href="{{ .URL | absLangURL }}" />
<meta property="dcterms:identifier" href="{{ .URL | absLangURL }}" />

{{- with .Title | default .Site.Title }}
<meta property="dcterms:title" content="{{ . }}" />
{{- end }}
<meta property="dcterms:creator" content="{{ .Params.author1 }}" />
{{- if .Params.author2 }}
<meta property="dcterms:creator" content="{{ .Params.author2 }}" />
{{- else }}
{{- end }}
<!-- Etc. -->
<meta property="dcterms:created" content="{{ .PublishDate.Format "2006-01-02" }}" />
<meta property="dcterms:abstract" content="{{ .Params.abstract | truncate 200 }}" xml:lang="{{ .Params.language }}" />
<meta property="dcterms:language" content="{{ .Params.language }}" />
<meta property="dcterms:type" content="{{ .Params.type }}" />
<meta property="dcterms:format" content="text/html" />
<meta property="dcterms:publisher" content="{{ .Params.publisher }}" />
{{- if .Keywords }}
    <meta property="dcterms:subject" content="{{ delimit .Keywords ", " }}" />
{{- else }}
{{- end }}
```
Cela permet d'exposer les métadonnées de l'entête de chacune des publications du site, ou des pages du site, sous la forme d'une structure RDF dans le code `HTML` sous la syntaxe _RDFa_.

# Limitations

- Dans l'instance d'Huma-Num il n'est pas possible d'avoir son propre nom de domaine.
